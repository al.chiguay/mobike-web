from django.shortcuts import render
from django.shortcuts import render, HttpResponse
from MobikeApp.forms import FormularioLogin
from django.views.generic.base import TemplateView

#-----------------------------------------------------------------------# 
class Registro(TemplateView):

    template_name = "MobikeApp/registro.html"
#-----------------------------------------------------------------------# 

def login(request):

    if request.method == "POST":
        miFormulario = FormularioLogin(request.POST)

        if miFormulario.is_valid():

            form=miFormulario.cleaned_data

            if form.get('usuario') == "admin":
                return render(request, "MobikeApp/admin.html")

            elif form.get('usuario') == "funcionario":
                return render(request, "Mobikeapp/funcionario.html")

            elif form.get('usuario') == "usuario":
                return render(request, "Mobikeapp/usuario.html")
            
            else:
                return render(request, "Mobikeapp/inicioMal.html")

    else:
        miFormulario = FormularioLogin()
        return render(request, "Mobikeapp/login.html", {"form": miFormulario})

#-----------------------------------------------------------------------# 


class AdminView(TemplateView):

    template_name = "MobikeApp/admin.html"

class AdminGestionarUsuario(TemplateView):
    template_name = "MobikeApp/admin-gestionar-usuario.html"
    
    


class AdminReportes(TemplateView):

    template_name = "MobikeApp/admin-reportes.html"

#-----------------------------------------------------------------------# 
class UsuarioView(TemplateView):
    template_name = "MobikeApp/usuario.html"

#-----------------------------------------------------------------------# 
class FuncionarioView(TemplateView):
    template_name = "MobikeApp/funcionario.html"

class FuncionarioReporte(TemplateView):
    template_name = "MobikeApp/funcionario-reportes.html"