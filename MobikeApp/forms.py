from django import forms

class FormularioLogin(forms.Form):
    usuario = forms.CharField(label="Nombre de usuario")
    password = forms.CharField(label="Password", widget=forms.PasswordInput, strip=False)